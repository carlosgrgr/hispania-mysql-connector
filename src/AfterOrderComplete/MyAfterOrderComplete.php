<?php

namespace HMCSource\AfterOrderComplete;

use HMCConfig\MyHMCConfig;

if (false === defined('ABSPATH')) {
    exit;
}

class MyAfterOrderComplete
{
    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        add_action('woocommerce_order_status_completed', [$this, 'db_insert_data'], 20, 1);
    }

    public function db_insert_data($order_id)
    {
        $fields = get_option('hispania_connection_options');

        if (empty($fields['active_connection'])) {
            return;
        }

        $order = wc_get_order($order_id);
        $field_names = '';
        $field_values = '';
        $id_web = 1 . $order_id;
        $online = 0;
        $hab_superior = 0;
        $procesado = 0;
        $fregistro = $order->get_date_created()->date("Y-m-d");
        $mod_pago = $order->get_payment_method_title();
        $notas = $order->get_customer_note();
        $imp_abonado = $order->get_payment_method_title() == 'Transferencia bancaria' ? 0 : $order->get_total();

        foreach ($fields as $field => $value) {
            if ($field == 'active_connection'){
                continue;
            }
            $field_names .= $field . ', ';
            $field_values .= '"';
            $value = get_post_meta($order_id, '_' . $value, true);
            if ($field == 'fecha_ini' || $field == 'fnac') {
                $date = \DateTime::createFromFormat('d/m/Y', $value);
                $value = $date ? $date->format('Y-m-d') : '';
            }
            $field_values .= $value;
            $field_values .= '"';
            $field_values .= ', ';
        }
        $field_names .= 'id_web, online, hab_superior, procesado, imp_abonado, mod_pago, notas, fregistro';
        $field_values .= '"' . $id_web . '", ' . $online . ', ' . $hab_superior . ', ' . $procesado . ', ' . $imp_abonado . ', "' . $mod_pago . '", "' . $notas . '", "' . $fregistro . '"';

        foreach ($order->get_items() as $item) {
            $product_id = $item->get_product_id();
            $product_attr = get_post_meta($product_id, '_product_attributes');

            $type = ! empty($product_attr) ? $product_attr[0]['tipo-curso']['value'] : '';
            $hours = ! empty($product_attr) ? $product_attr[0]['horas']['value'] : 0;
            $imp_curso = $item->get_total();
            
            $field_names .= ', tipo_curso, horas, imp_curso';
            $field_values .= ', "' . $type . '", ' . $hours . ', ' . $imp_curso;
            $db = \mysqli_connect('hispania-valencia.es', 'usu_bd_his_es', '@bmh259W%qw', 'bd_his_es');
            $db->set_charset("utf8");
            $query = 'INSERT INTO tabla_inserta (' . $field_names . ') values (' . $field_values . ');';
            $result[] = \mysqli_query($db, $query);
        }

        if (in_array(false, $result)) {
            MyHMCConfig::add_flash_notice(__('Se ha producido algún error al actualizar el registro en la base de datos "bd_his_es".'), 'error', false);
            return;
        }
        MyHMCConfig::add_flash_notice(__('Se ha actualizado el registro en la base de datos "bd_his_es".'), 'info', false);
        return;
    }
}
