<?php

namespace HMCSource\AdminSettings;

if (false === defined('ABSPATH')) {
    exit;
}

class MyAdminSettings
{

    protected $excluded_columns = [
        'id', 
        'id_web', 
        'fregistro',
        'online',
        'procesado',
        'tipo_curso',
        'horas',
        'imp_curso',
        'imp_abonado',
        'mod_pago',
        'notas',
        'hab_superior'
    ];

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->init_filters();
        $this->init_actions();
    }
    
    public function init_filters()
    {
        add_filter('plugin_action_links_hispania-mysql-connector/hispania-mysql-connector.php', [$this, 'add_settings_link_in_plugins_page']);
        
    }
    
    public function init_actions()
    {
        add_action('admin_menu', [$this, 'create_wc_submenu_settings_page'], 9999);
        add_action('admin_init', [$this, 'create_settings_and_fields']);

    }

    public function add_settings_link_in_plugins_page($links) 
    {
        $url = esc_url(add_query_arg('page', 'hispania-connection-settings', get_admin_url() . 'admin.php'));
        $settings_link = "<a href='$url'>" . __( 'Settings' ) . '</a>';
        array_push($links, $settings_link);

        return $links;
    }

    public function create_wc_submenu_settings_page()
    {
        add_submenu_page('woocommerce', __('Hispania ajustes de conexión', HMC_TEXTDOMAIN), __('Hispania ajustes de conexión', HMC_TEXTDOMAIN), 'manage_options', 'hispania-connection-settings', [$this, 'render_hc_settings_page_content'], 9999);
    }

    // Register our settings. Add the settings section, and settings fields
    public function create_settings_and_fields()
    {
        $this->add_settings_fields();
        register_setting('hispania_connection_options', 'hispania_connection_options', [$this, 'selected_options_validate']);
        add_settings_section('main_section', __('Emparejar datos', HMC_TEXTDOMAIN), '__return_false', __FILE__);
    }
    
    public function selected_options_validate($input) {
        foreach ($input as $db_column_name => $awcfe_field_name) {
            $input[$db_column_name] =  wp_filter_nohtml_kses($input[$db_column_name]);	
        }

        return $input;
    }
    
    public function add_settings_fields()
    {
        $fields = $this->get_database_fields();
        $db_data = get_option('hispania_connection_options');
        $fields_categories = $this->get_awcfe_fields();

        add_settings_field('active_connection', __('Activar conexión', HMC_TEXTDOMAIN), [$this, 'render_checkbox_field'], __FILE__, 'main_section', $db_data);
        foreach ($fields as $field) {
            $args = [
                'fields_categories' => $fields_categories,
                'field' => $field,
                'db_data' => $db_data[$field['name']],
            ];
            add_settings_field($field['name'], $field['label'], [$this, 'render_dropdown_field'], __FILE__, 'main_section', $args);
        }
    }

    /**
     * Get database columns names
     * 
     * @return array
     */
    public function get_database_fields()
    {
        $columns = $this->get_database_columns();
        foreach ($columns as $column) {
            $field = [
                'label' => esc_html($column),
                'type' => 'field_select',
                'name' => esc_html($column),
            ];
            $fields[] = $field;
        }
        return $fields;
    }

    /**
     * Get database columns names
     * 
     * @return array
     */
    public function get_database_columns()
    {
        $db         = \mysqli_connect('hispania-valencia.es', 'usu_bd_his_es', '@bmh259W%qw', 'bd_his_es');
        $query      = "SELECT * FROM tabla_inserta";
        $result     = \mysqli_query($db, $query);
        $info 		= $result->fetch_fields();
        $columns 	= array_column($info, 'name');
        $result->free();
        $db->close();
        $columns = $this->exclude_columns($columns);
        return $columns;
    }

    /**
     * Exclude columns from array from database
     *
     * @param array $columns
     * @return array
     */
    public function exclude_columns(array $columns)
    {
        $to_exclude = $this->excluded_columns;
        foreach ($to_exclude as $exclude) {
            if ($key = array_search($exclude, $columns)) {
                array_splice($columns, $key, 1);
            }
        }
        return $columns;
    }

    public function render_checkbox_field($args)
    {    
        $checked = !empty($args['active_connection']) ? ' checked="checked" ' : '';
        echo '<input ' . $checked . ' id="active_connection" name="hispania_connection_options[active_connection]" type="checkbox" value="1" />';
    }
    
    public function render_dropdown_field($args)
    {    
        $fields_categories = $args['fields_categories'];

        echo "<select id='wc_hispania_connect' name='hispania_connection_options[" . $args['field']['name'] . "]'>";
        echo "<option value=''></option>";
        foreach ($fields_categories as $cat_name => $fields) {
            foreach ($fields as $item) {
                $selected = (! empty($args['db_data']) && $args['db_data'] == $item['name']) ? 'selected="selected"' : '';
                echo "<option value='" . $item['name'] . "' {$selected}>" . __(ucfirst($cat_name), 'woocommerce') . ' ' . $item['label'] . '</option>';
            }
        }
        echo "</select>";
    }

    public function render_hc_settings_page_content()
    {
    ?>
        <div class="wrap">
            <div class="icon32" id="icon-options-general"><br></div>
            <h2><?php _e('Conexión con software de Hispania', HMC_TEXTDOMAIN) ?></h2>
            <form action="options.php" method="post">
                <?php if ( function_exists('wp_nonce_field') ) {
                    // wp_nonce_field('plugin-name-action_' . "yep");
                } ?>
                <?php settings_fields('hispania_connection_options'); ?>
                <?php do_settings_sections(__FILE__); ?>
                <p class="submit">
                    <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Guardar cambios', HMC_TEXTDOMAIN); ?>" />
                </p>
            </form>
        </div>
    <?php
    }

    /**
     * Get awcfe fields
     * 
     * @return array
     */
    public function get_awcfe_fields()
    {
        $customSections = get_option('awcfe_fields');

        if ($customSections && isset($customSections['fields'])) {
            $customSections = $customSections['fields'];
        } else {
            $customSections = [];
        }

        $customSections = $this->spreadRowsCols($customSections);
        
        $customSections = array_map(
            function ($section) {
                $newArr = $section;

                foreach ($section as $key => $field) {

                    if (isset($field['active']) && $field['active'] === false) {
                        unset($newArr[$key]);
                    } else {

                        if (isset($field['custom_class'])) {
                            $custom_class = preg_split('/[\ \n\,]+/', $field['custom_class']);
                            $newArr[$key]['class'] = @array_merge($newArr[$key]['class'], $custom_class);
                        }

                        $makeinline = ( isset($field['makeinline']) && $field['makeinline'] == 1 ) ? array('awcfe-inline-item') : array();
                        // if (isset($newArr[$key]['class'])) {
                        //     $newArr[$key]['class'] = array_merge($newArr[$key]['class'], $makeinline);
                        // }

                        if (isset($field['type'])) {
                            if ($field['type'] === 'tel') {
                                $newArr[$key]['validate'] = array_merge(isset($newArr[$key]['validate']) ? $newArr[$key]['validate'] : [], ['phone']);
                            }
                            if ($field['type'] === 'email') {
                                $newArr[$key]['validate'] = array_merge(isset($newArr[$key]['validate']) ? $newArr[$key]['validate'] : [], ['email']);
                            }
                            //
                        }
                        if (isset($field['options']) && is_array($field['options'])) {

                          if(!empty($field['name']) && $field['name'] == 'address_book'){
                            unset($newArr[$key]['options']);
                            // $addrOpts = $this->awcfe_getAddressBook_plugin();
                            if( !empty( $addrOpts )){
                              foreach($addrOpts as $ka => $va){
                                $newArr[$key]['options'][$ka] = $va;
                              }
                            }
                          } else {
                            $newArr[$key]['options'] = [];
                            foreach ($field['options'] as $option) {
                                @$newArr[$key]['options'][$option['value']] = @$option['label'];
                                if (isset($option['selected']) && $option['selected'] === true) {
                                    $newArr[$key]['default'] = $option['value'];
                                }
                            }

                          }
                        }
                        if (isset($field['rows'])) {
                            $newArr[$key]['custom_attributes'] = array_merge(isset($newArr[$key]['custom_attributes']) ? $newArr[$key]['custom_attributes'] : [], ['rows' => $field['rows']]);
                        }
                    }


                }

                return $newArr;

            },
            $customSections
        );
        return $customSections;
    }

    public function spreadRowsCols($sections)
    {
        $sections = array_map(
            function ($section) {
                $newAr = array();
                if (isset($section['fields'][0][0])) {// check if rowXcol arrange ment
                    foreach ($section['fields'] as $row => $col) {
                        foreach ($col as $i => $v) {
                            // check if disabled and exclude
                            if (isset($v['name'])) {
                                $key = $v['name'];
                            } else if (isset($v['elementId'])) {
                                $key = $v['elementId'];
                            } else {
                                continue;
                            }
                            if (empty($key)) {
                                continue;
                            }
                            $index = $i;
                            $newAr[$key] = $v;
                            $newAr[$key]['class'] = '';
                            if ($v['type'] === 'text') {
                                $newAr[$key]['type'] = $v['subtype'];
                            }

                        }

                    }
                }

                return $newAr;
            },
            $sections
        );
        return $sections;
    }

    public function address_select_label( $address, $name ) {
        $label = '';

        $address_nickname = get_user_meta( get_current_user_id(), $name . '_address_nickname', true );
        if ( $address_nickname ) {
            $label .= $address_nickname . ': ';
        }

        if ( ! empty( $address[ $name . '_first_name' ] ) ) {
            $label .= $address[ $name . '_first_name' ];
        }
        if ( ! empty( $address[ $name . '_last_name' ] ) ) {
        if ( ! empty( $label ) ) {
            $label .= ' ';
        }
        $label .= $address[ $name . '_last_name' ];
        }
        if ( ! empty( $address[ $name . '_address_1' ] ) ) {
        if ( ! empty( $label ) ) {
            $label .= ', ';
        }
        $label .= $address[ $name . '_address_1' ];
        }
        if ( ! empty( $address[ $name . '_city' ] ) ) {
        if ( ! empty( $label ) ) {
            $label .= ', ';
        }
        $label .= $address[ $name . '_city' ];
        }
        if ( ! empty( $address[ $name . '_state' ] ) ) {
        if ( ! empty( $label ) ) {
            $label .= ', ';
        }
        $label .= $address[ $name . '_state' ];
        }

        return apply_filters( 'wc_address_book_address_select_label', $label, $address, $name );
    }

}