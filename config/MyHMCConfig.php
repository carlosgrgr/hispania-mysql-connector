<?php

namespace HMCConfig;

if (false === defined('ABSPATH')) {
    exit;
}

class MyHMCConfig
{
    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        add_action('admin_notices', [$this, 'display_flash_notices'], 12);
    }

    /**
    * Check if WooCommerce is active (including network activated)
    **/
    public function is_wc_active()
    {
        $plugin_path = trailingslashit(WP_PLUGIN_DIR) . 'woocommerce/woocommerce.php';

        if (in_array($plugin_path, wp_get_active_and_valid_plugins()) || in_array($plugin_path, wp_get_active_network_plugins())) {
            return true;
        }
        return false;
    }

    public static function add_flash_notice($notice = '', $type = 'warning', $dismissible = true)
    {
        $notices = get_option('my_flash_notices', []);
        $dismissible_text = ($dismissible) ? 'is-dismissible' : '';
        array_push($notices, [
            'notice' => $notice,
            'type' => $type,
            'dismissible' => $dismissible_text
        ]);
        update_option('my_flash_notices', $notices);
    }

    public function display_flash_notices()
    {
        $notices = get_option('my_flash_notices', []);
        foreach ($notices as $notice) {
            printf(
                '<div class="notice notice-%1$s %2$s"><p>%3$s</p></div>',
                $notice['type'],
                $notice['dismissible'],
                $notice['notice']
            );
        }
        if (!empty($notices)) {
            delete_option('my_flash_notices', []);
        }
    }
}
