<?php

/**
 * Plugin Name: Hispania MySQL Connector
 * Plugin URI: https://bitbucket.org/carlosgrgr/hispania-mysql-connector
 * Description: Developed to Hispania, escuela de español. Connect Woocommerce orders and gravity form to a third database.
 * Author: Carlos García Rodríguez
 * Author URI: https://carlosgrgr.es
 * Version: 1.0
 * Date: 2021-07-21
 * License: GPLv2
 * Text Domain: hispania_mysql_connector
 * Domain Path: /languages
 */

if (false === defined('ABSPATH')) {
    exit;
}

require_once 'vendor/autoload.php';

define('HMC_PLUGIN_FILE', __FILE__);
define('HMC_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('HMC_PLUGIN_URL', plugin_dir_url(__FILE__));
define('HMC_PLUGIN_ENDPOINTS_NAMESPACE', 'hispania-mysql-connector');
define('HMC_TEXTDOMAIN', 'hispania-mysql-connector');

use HMCConfig\MyHMCConfig;
use HMCSource\AdminSettings\MyAdminSettings;
use HMCSource\AfterOrderComplete\MyAfterOrderComplete;

$config = new MyHMCConfig();

if ($config->is_wc_active()) {
    $afterCompleteOrder = new MyAfterOrderComplete();
    $plugin_settings = new MyAdminSettings();
}
